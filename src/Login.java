import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.csv.CSVParser;

public class Login extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String login = request.getParameter("email");
        String pass = request.getParameter("pass");
        out.println(login);
        out.println(pass);

        if(Validate.checkUser(login, pass, false, out))
        {
            HttpSession session = request.getSession();
            if (session.getAttribute("user") == null) {
                session.setAttribute("user", 1);
            }
            RequestDispatcher rs = request.getRequestDispatcher("Welcome");
            rs.forward(request, response);
        }
        else
        {
            HttpSession session = request.getSession();
            if (session.getAttribute("user") != null) {
                session.removeAttribute("user");
            }
            out.println("Username or Password incorrect");
            RequestDispatcher rs = request.getRequestDispatcher("index.html");
            rs.include(request, response);
        }
    }
}