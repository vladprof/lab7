import java.io.IOException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.PrintWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Validate {
    public static boolean checkUser(String login, String pass, boolean reg_check, PrintWriter out) throws IOException {
        boolean passed = false;
        try (
                Reader reader = Files.newBufferedReader(Paths.get("users.csv"));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        ) {
            for (CSVRecord csvRecord : csvParser) {
                if (login.equals(csvRecord.get(0)) && (reg_check || pass.equals(csvRecord.get(1)))) {
                    passed = true;
                }
            }
        }
        return passed;
    }
}