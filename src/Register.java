import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class Register extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String login = request.getParameter("login");
        String pass = request.getParameter("pass");

        if (!Validate.checkUser(login, pass, true, out)) {
            try (
                    BufferedWriter writer = Files.newBufferedWriter(Paths.get("users.csv"), StandardOpenOption.APPEND);
                    CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            ) {
                csvPrinter.printRecord(login, pass);
                csvPrinter.flush();
                out.print("You are successfully registered...<br><a href=\"index.html\">Login</a>");
            } catch (Exception e2) {
                System.out.println(e2);
            }
        } else {
            out.print("User already exists!");
        }

        out.close();
    }

}