var nHeight = 0;
var nWidth = 0;
$(document).ready(function () {
    $('#image').change(function () {
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var file = $(this).prop('files')[0];

        if ($.inArray(file['type'], validImageTypes) < 0){
            resetForm('Файл не является изображением!');
        } else {
            var badWords = ['script', 'http', 'SELECT', 'UNION', 'UPDATE', 'exe', 'exec', 'INSERT', 'tmp', '_&&_'];
            var has_bads = false;

            $.each(badWords, function (i, word) {
                if (file['name'].indexOf(word) !== -1){
                    has_bads = true;
                }
            });
            if (has_bads){
                resetForm('Название файла содержит запрещённые фразы!');
            } else if(file.size > 2*1024*1024){
                resetForm('Вес файла превышает 2 МБ!');
            } else {
                var img = new Image();
                var _URL = window.URL || window.webkitURL;

                img.src = _URL.createObjectURL(file);
                img.onload = function() {
                    console.log(this.naturalWidth);
                    console.log(this.naturalHeight);
                    nHeight = this.naturalHeight;
                    nWidth = this.naturalWidth;
                    if (this.naturalWidth > 800 || this.naturalHeight > 800){
                        resetForm('Размер изображения превышет 800 пикселей по одной из сторон!');
                    } else {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#preview').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);
                    }
                };
                $('#submit').prop('disabled', '');
            }
        }
    });
    $('#preview').click(function () {
       if (nWidth !== parseInt($('#preview').attr('Width'))){
           $('#preview').attr('Width', nWidth);
           $('#preview').attr('Height', nHeight);
       } else {
           $('#preview').attr('Width', 200);
           $('#preview').attr('Height', 200);
       }
    });
});

function resetForm(text) {
    $('#gallery_form').trigger('reset');
    $('#preview').attr('src', '');
    $('#submit').prop('disabled', 'disabled');
    if(text !== undefined) alert(text);
}