<%@ page contentType="text/html; charset=UTF-8" %>
<%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("../index.html");
    }
%>
<head>
    <script src="jquery/jquery-3.3.1.min.js"></script>
    <script src="js/gallery.js"></script>
    <meta charset="UTF-8">
</head>
<div class="content-margin">
    <div class="container-fluid row">
        <div class="col-sm-6">
            <form action="Upload" method="post" id="gallery_form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleFormControlFile1"><b>Изображение</b></label>
                    <input name="image" type="file" class="form-control-file" id="image">
                </div>
                <input id="submit" type="submit" name="submit" value="Submit" class="btn btn-primary" disabled>
            </form>
        </div>
        <div class="col-sm-6">
            <b>Предпросмотр</b>
            <img id="preview" class="img-fluid" width="200" height="200" src="">
        </div>
        <a href="../Logout">Выход</a>
    </div>
</div>

